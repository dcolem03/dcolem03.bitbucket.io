var annotated_dup =
[
    [ "Bluetooth", null, [
      [ "Bluetooth", "classBluetooth_1_1Bluetooth.html", "classBluetooth_1_1Bluetooth" ]
    ] ],
    [ "BT_LED_Task", null, [
      [ "TaskBTLED", "classBT__LED__Task_1_1TaskBTLED.html", "classBT__LED__Task_1_1TaskBTLED" ]
    ] ],
    [ "ClosedLoop", null, [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoop_Lab7", null, [
      [ "ClosedLoop", "classClosedLoop__Lab7_1_1ClosedLoop.html", "classClosedLoop__Lab7_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask", null, [
      [ "TaskController", "classControllerTask_1_1TaskController.html", "classControllerTask_1_1TaskController" ]
    ] ],
    [ "ControllerTask_Lab7", null, [
      [ "TaskController", "classControllerTask__Lab7_1_1TaskController.html", "classControllerTask__Lab7_1_1TaskController" ]
    ] ],
    [ "Elevator_FSM", null, [
      [ "Button", "classElevator__FSM_1_1Button.html", "classElevator__FSM_1_1Button" ],
      [ "MotorDriver", "classElevator__FSM_1_1MotorDriver.html", "classElevator__FSM_1_1MotorDriver" ],
      [ "TaskElevator", "classElevator__FSM_1_1TaskElevator.html", "classElevator__FSM_1_1TaskElevator" ]
    ] ],
    [ "Encoder", null, [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Encoder_Task", null, [
      [ "TaskEncoder", "classEncoder__Task_1_1TaskEncoder.html", "classEncoder__Task_1_1TaskEncoder" ]
    ] ],
    [ "EncoderDriver", null, [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "EncoderDriver_Lab7", null, [
      [ "EncoderDriver", "classEncoderDriver__Lab7_1_1EncoderDriver.html", "classEncoderDriver__Lab7_1_1EncoderDriver" ]
    ] ],
    [ "LED_FSM", null, [
      [ "RealLED", "classLED__FSM_1_1RealLED.html", "classLED__FSM_1_1RealLED" ],
      [ "TaskLED", "classLED__FSM_1_1TaskLED.html", "classLED__FSM_1_1TaskLED" ],
      [ "VirtualLED", "classLED__FSM_1_1VirtualLED.html", "classLED__FSM_1_1VirtualLED" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "MotorDriver_Lab7", null, [
      [ "MotorDriver", "classMotorDriver__Lab7_1_1MotorDriver.html", "classMotorDriver__Lab7_1_1MotorDriver" ]
    ] ],
    [ "Nucleo_Task", null, [
      [ "TaskNucleo", "classNucleo__Task_1_1TaskNucleo.html", "classNucleo__Task_1_1TaskNucleo" ]
    ] ],
    [ "UI_Task", null, [
      [ "TaskUI", "classUI__Task_1_1TaskUI.html", "classUI__Task_1_1TaskUI" ]
    ] ]
];