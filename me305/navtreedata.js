/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 305 Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Overview", "page_overview.html", [
      [ "Lab 0x01: Fibonacci", "page_overview.html#lab_1", null ],
      [ "Homework 0x01: Elevator FSM", "page_overview.html#Hw_1", null ],
      [ "Lab 0x02: LED FSM", "page_overview.html#lab_2", null ],
      [ "Lab 0x03: Encoder", "page_overview.html#lab_3", null ],
      [ "Lab 0x04: Extended User Interface", "page_overview.html#lab_4", null ],
      [ "Lab 0x05: Use Your Interface", "page_overview.html#lab_5", null ],
      [ "Lab 0x06: DC Motor", "page_overview.html#lab_6", null ],
      [ "Lab 0x07: Reference Tracking", "page_overview.html#lab_7", null ]
    ] ],
    [ "Lab 1: Fibonacci", "page_fibonacci.html", [
      [ "Introduction", "page_fibonacci.html#page_fibonacci_int", null ],
      [ "Source Code Access", "page_fibonacci.html#page_fib_src", null ],
      [ "Documentation", "page_fibonacci.html#page_fib_doc", null ]
    ] ],
    [ "Homework 1: Elevator FSM", "page_elevator.html", [
      [ "Introduction", "page_elevator.html#page_elevator_int", null ],
      [ "State Transition Diagram", "page_elevator.html#page_elev_fsm", null ],
      [ "Source Code Access", "page_elevator.html#page_elev_src", null ],
      [ "Documentation", "page_elevator.html#page_elev_doc", null ]
    ] ],
    [ "Lab 2: LED FSM", "page_LED.html", [
      [ "Introduction", "page_LED.html#page_LED_int", null ],
      [ "State Transition Diagram", "page_LED.html#page_LED_fsm", null ],
      [ "Source Code Access", "page_LED.html#page_LED_src", null ],
      [ "Documentation", "page_LED.html#page_LED_doc", null ]
    ] ],
    [ "Lab 3: Encoder", "page_Encoder.html", [
      [ "Introduction", "page_Encoder.html#page_Encoder_int", null ],
      [ "User Interface State Transition Diagram", "page_Encoder.html#page_Encoder_uifsm", null ],
      [ "Encoder State Transition Diagram", "page_Encoder.html#page_Encoder_efsm", null ],
      [ "Task Diagram", "page_Encoder.html#page_Encoder_tsk", null ],
      [ "Source Code Access", "page_Encoder.html#page_Encoder_src", null ],
      [ "Documentation", "page_Encoder.html#page_Encoder_doc", null ]
    ] ],
    [ "Lab 4: Extended User Interface", "page_XUI.html", [
      [ "Introduction", "page_XUI.html#page_XUI_int", null ],
      [ "State Transition Diagram", "page_XUI.html#page_XUI_fsm", null ],
      [ "Source Code Access", "page_XUI.html#page_XUI_src", null ],
      [ "Documentation", "page_XUI.html#page_XUI_doc", null ]
    ] ],
    [ "Lab 5: Use Your Interface", "page_UYI.html", [
      [ "Introduction", "page_UYI.html#page_UYI_int", null ],
      [ "State Transition Diagram", "page_UYI.html#page_UYI_fsm", null ],
      [ "Task Diagram", "page_UYI.html#page_LED_tsk", null ],
      [ "Source Code Access", "page_UYI.html#page_UYI_src", null ],
      [ "Documentation", "page_UYI.html#page_UYI_doc", null ]
    ] ],
    [ "Lab 6: DC Motors", "page_DCM.html", [
      [ "Introduction", "page_DCM.html#page_DCM_int", null ],
      [ "State Transition Diagram", "page_DCM.html#page_DCM_fsm", null ],
      [ "Task Diagram", "page_DCM.html#page_DCM_tsk", null ],
      [ "Controller Tuning", "page_DCM.html#page_DCM_tun", null ],
      [ "Kp = 0.2", "page_DCM.html#page_DCM_kpa", null ],
      [ "Kp = 0.6", "page_DCM.html#page_DCM_kpb", null ],
      [ "Kp = 0.8", "page_DCM.html#page_DCM_kpc", null ],
      [ "Kp = 1", "page_DCM.html#page_DCM_kpd", null ],
      [ "Source Code Access", "page_DCM.html#page_DCM_src", null ],
      [ "Documentation", "page_DCM.html#page_DCM_doc", null ]
    ] ],
    [ "Lab 7: Reference Tracking", "page_RFT.html", [
      [ "Introduction", "page_RFT.html#page_RFT_int", null ],
      [ "State Transition Diagram", "page_RFT.html#page_RFT_fsm", null ],
      [ "Task Diagram", "page_RFT.html#page_RFT_tsk", null ],
      [ "Performance", "page_RFT.html#page_RFT_prf", null ],
      [ "Source Code Access", "page_RFT.html#page_RFT_src", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BT__LED__Task_8py.html",
"classEncoder_1_1Encoder.html#ac9a8588368c88ba9513417ee95ba8145"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';