var classControllerTask__Lab7_1_1TaskController =
[
    [ "__init__", "classControllerTask__Lab7_1_1TaskController.html#ae4dd2d63a9da0ab81b7422d39ff07241", null ],
    [ "printTrace", "classControllerTask__Lab7_1_1TaskController.html#ad16d9816901cec831f3e9418ec0d89fa", null ],
    [ "run", "classControllerTask__Lab7_1_1TaskController.html#a5151555b29b63759be9b28b012b51666", null ],
    [ "transitionTo", "classControllerTask__Lab7_1_1TaskController.html#a9c543e2b0302a4f9c92f0d58ac92a1b5", null ],
    [ "CL", "classControllerTask__Lab7_1_1TaskController.html#a25ab76aa7106867de865f3d9aeffbf07", null ],
    [ "curr_time", "classControllerTask__Lab7_1_1TaskController.html#a4818240393d4072204d8e8ac0b941665", null ],
    [ "dbg", "classControllerTask__Lab7_1_1TaskController.html#a33bea482904abbd960cae026cbdf3f46", null ],
    [ "encoder", "classControllerTask__Lab7_1_1TaskController.html#a9b9b305d3fd151dbad5f7791a847c9f0", null ],
    [ "index", "classControllerTask__Lab7_1_1TaskController.html#a54dc281ed98a185133f654c1bfd7d9f6", null ],
    [ "interval", "classControllerTask__Lab7_1_1TaskController.html#a388f3a0b6bb9500e0e6eb21a2ea3ddfe", null ],
    [ "J", "classControllerTask__Lab7_1_1TaskController.html#a409fd9a9114ca7fc5f0dde0af35a55fa", null ],
    [ "Kp", "classControllerTask__Lab7_1_1TaskController.html#a106d36303e25bfb8d27c97bfc7ec0f59", null ],
    [ "motor", "classControllerTask__Lab7_1_1TaskController.html#a27d0d8bb7b44891b0a5d97487322f9fa", null ],
    [ "next_time", "classControllerTask__Lab7_1_1TaskController.html#ace7aa394f2f54cf5b84102ebcdabe1c5", null ],
    [ "Om_ref", "classControllerTask__Lab7_1_1TaskController.html#a499f473c2971b28e826c29bdf6dc27c0", null ],
    [ "position", "classControllerTask__Lab7_1_1TaskController.html#a125bf08f04bbd679b7d35a871f5717c5", null ],
    [ "runs", "classControllerTask__Lab7_1_1TaskController.html#ae4e0be0b2c3c675d229c20d629bc080d", null ],
    [ "ser", "classControllerTask__Lab7_1_1TaskController.html#a9ad4c1dc93f7efa52332d37859214e45", null ],
    [ "start", "classControllerTask__Lab7_1_1TaskController.html#a064574487a174b54fa80f61b516b6353", null ],
    [ "start_time", "classControllerTask__Lab7_1_1TaskController.html#abd3ac7d15fdb3d390121db76eb8c76ad", null ],
    [ "state", "classControllerTask__Lab7_1_1TaskController.html#a579da40718a6e1444addf8042cd89b54", null ],
    [ "T_ref", "classControllerTask__Lab7_1_1TaskController.html#a11bd1441a2b5c0a53ea026f64c798cd0", null ],
    [ "taskNum", "classControllerTask__Lab7_1_1TaskController.html#a9bc01220127f03fc9e252db2c8f2463d", null ],
    [ "Th_ref", "classControllerTask__Lab7_1_1TaskController.html#a924a1590e5b678189092d9cbe3841df8", null ],
    [ "velocity", "classControllerTask__Lab7_1_1TaskController.html#ae1a4f537c14bb82399651b585b3e14b7", null ]
];