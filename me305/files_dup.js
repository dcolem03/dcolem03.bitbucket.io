var files_dup =
[
    [ "Bluetooth.py", "Bluetooth_8py.html", [
      [ "Bluetooth", "classBluetooth_1_1Bluetooth.html", "classBluetooth_1_1Bluetooth" ]
    ] ],
    [ "BT_LED_Task.py", "BT__LED__Task_8py.html", [
      [ "TaskBTLED", "classBT__LED__Task_1_1TaskBTLED.html", "classBT__LED__Task_1_1TaskBTLED" ]
    ] ],
    [ "BTmain.py", "BTmain_8py.html", "BTmain_8py" ],
    [ "ClosedLoop.py", "ClosedLoop_8py.html", [
      [ "ClosedLoop", "classClosedLoop_1_1ClosedLoop.html", "classClosedLoop_1_1ClosedLoop" ]
    ] ],
    [ "ClosedLoop_Lab7.py", "ClosedLoop__Lab7_8py.html", [
      [ "ClosedLoop", "classClosedLoop__Lab7_1_1ClosedLoop.html", "classClosedLoop__Lab7_1_1ClosedLoop" ]
    ] ],
    [ "ControllerTask.py", "ControllerTask_8py.html", [
      [ "TaskController", "classControllerTask_1_1TaskController.html", "classControllerTask_1_1TaskController" ]
    ] ],
    [ "ControllerTask_Lab7.py", "ControllerTask__Lab7_8py.html", [
      [ "TaskController", "classControllerTask__Lab7_1_1TaskController.html", "classControllerTask__Lab7_1_1TaskController" ]
    ] ],
    [ "Elevator_FSM.py", "Elevator__FSM_8py.html", [
      [ "TaskElevator", "classElevator__FSM_1_1TaskElevator.html", "classElevator__FSM_1_1TaskElevator" ],
      [ "Button", "classElevator__FSM_1_1Button.html", "classElevator__FSM_1_1Button" ],
      [ "MotorDriver", "classElevator__FSM_1_1MotorDriver.html", "classElevator__FSM_1_1MotorDriver" ]
    ] ],
    [ "Elevator_FSM_main.py", "Elevator__FSM__main_8py.html", "Elevator__FSM__main_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "Encoder_Main.py", "Encoder__Main_8py.html", "Encoder__Main_8py" ],
    [ "Encoder_Task.py", "Encoder__Task_8py.html", [
      [ "TaskEncoder", "classEncoder__Task_1_1TaskEncoder.html", "classEncoder__Task_1_1TaskEncoder" ]
    ] ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "EncoderDriver_Lab7.py", "EncoderDriver__Lab7_8py.html", [
      [ "EncoderDriver", "classEncoderDriver__Lab7_1_1EncoderDriver.html", "classEncoderDriver__Lab7_1_1EncoderDriver" ]
    ] ],
    [ "Lab0x01.py", "Lab0x01_8py.html", "Lab0x01_8py" ],
    [ "Lab4_UART.py", "Lab4__UART_8py.html", "Lab4__UART_8py" ],
    [ "Lab6_main.py", "Lab6__main_8py.html", "Lab6__main_8py" ],
    [ "LED_FSM.py", "LED__FSM_8py.html", [
      [ "TaskLED", "classLED__FSM_1_1TaskLED.html", "classLED__FSM_1_1TaskLED" ],
      [ "RealLED", "classLED__FSM_1_1RealLED.html", "classLED__FSM_1_1RealLED" ],
      [ "VirtualLED", "classLED__FSM_1_1VirtualLED.html", "classLED__FSM_1_1VirtualLED" ]
    ] ],
    [ "LED_FSM_main.py", "LED__FSM__main_8py.html", "LED__FSM__main_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_Lab7.py", "main__Lab7_8py.html", "main__Lab7_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "MotorDriver_Lab7.py", "MotorDriver__Lab7_8py.html", [
      [ "MotorDriver", "classMotorDriver__Lab7_1_1MotorDriver.html", "classMotorDriver__Lab7_1_1MotorDriver" ]
    ] ],
    [ "Nucleo_Task.py", "Nucleo__Task_8py.html", [
      [ "TaskNucleo", "classNucleo__Task_1_1TaskNucleo.html", "classNucleo__Task_1_1TaskNucleo" ]
    ] ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "UI_Task.py", "UI__Task_8py.html", [
      [ "TaskUI", "classUI__Task_1_1TaskUI.html", "classUI__Task_1_1TaskUI" ]
    ] ],
    [ "UIFrontEnd.py", "UIFrontEnd_8py.html", "UIFrontEnd_8py" ],
    [ "UIFrontEnd_Lab7.py", "UIFrontEnd__Lab7_8py.html", "UIFrontEnd__Lab7_8py" ]
];