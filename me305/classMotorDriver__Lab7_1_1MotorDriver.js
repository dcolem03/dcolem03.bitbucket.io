var classMotorDriver__Lab7_1_1MotorDriver =
[
    [ "__init__", "classMotorDriver__Lab7_1_1MotorDriver.html#aaf57a95f49c5ce4f69bbcd95454e43e0", null ],
    [ "disable", "classMotorDriver__Lab7_1_1MotorDriver.html#a6ebbe0f94812987f78a20752475a9da5", null ],
    [ "enable", "classMotorDriver__Lab7_1_1MotorDriver.html#ad67e891deb0b3b657274bec3f989fc64", null ],
    [ "set_duty", "classMotorDriver__Lab7_1_1MotorDriver.html#a4eddaddaf9a7669c31b9222aad232b92", null ],
    [ "pin_IN1", "classMotorDriver__Lab7_1_1MotorDriver.html#ab022c3313efcf98626104cccced923cb", null ],
    [ "pin_IN2", "classMotorDriver__Lab7_1_1MotorDriver.html#a476e9253ffa7fd6f278b3cc5b6e4d71d", null ],
    [ "pin_nSLEEP", "classMotorDriver__Lab7_1_1MotorDriver.html#ae343ac81dc961e31c8ee1f784317e503", null ],
    [ "tch1", "classMotorDriver__Lab7_1_1MotorDriver.html#ac813f7719ba490f7e341c09cf4099374", null ],
    [ "tch2", "classMotorDriver__Lab7_1_1MotorDriver.html#a9104c70928b1b4a19ee8cc75601df424", null ]
];