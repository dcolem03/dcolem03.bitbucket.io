/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME 405 Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Overview", "page_overview.html", [
      [ "Lab 0x01: Vending Machine FSM", "page_overview.html#lab_1", null ],
      [ "Lab 0x02: Think Fast", "page_overview.html#lab_2", null ],
      [ "Lab 0x03: Pushing the Right Buttons", "page_overview.html#lab_3", null ],
      [ "Lab 0x04: Hot or Not?", "page_overview.html#lab_4", null ],
      [ "Lab 0x05: Feeling Tipsy?", "page_overview.html#lab_5", null ],
      [ "Lab 0x06: Simulation or Reality?", "page_overview.html#lab_6", null ],
      [ "Lab 0x07: Feeling Touchy", "page_overview.html#lab_7", null ],
      [ "Lab 0x08 & 0x09: Term Project", "page_overview.html#lab_8_9", null ]
    ] ],
    [ "Lab1: Vending Machine", "page_vend.html", [
      [ "Introduction", "page_vend.html#page_vend_int", null ],
      [ "State Transition Diagram", "page_vend.html#page_vend_std", null ],
      [ "Source Code Access", "page_vend.html#page_vend_src", null ],
      [ "Documentation", "page_vend.html#sec_doc", null ]
    ] ],
    [ "Lab2: Think Fast", "page_react.html", [
      [ "Introduction", "page_react.html#page_react_int", null ],
      [ "Source Code Access", "page_react.html#page_react_src", null ],
      [ "Documentation", "page_react.html#page_react_doc", null ]
    ] ],
    [ "Lab3: Pushing the Right Buttons", "page_btn.html", [
      [ "Introduction", "page_btn.html#page_btn_int", null ],
      [ "Sample Step Response Plot", "page_btn.html#page_btn_plt", null ],
      [ "Source Code Access", "page_btn.html#page_btn_src", null ]
    ] ],
    [ "Lab4: Hot or Not?", "page_hot.html", [
      [ "Introduction", "page_hot.html#page_hot_int", null ],
      [ "Sample Temperature Data", "page_hot.html#page_hot_plt", null ],
      [ "Sample Data", "page_hot.html#page_hot_dta", null ],
      [ "Source Code Access", "page_hot.html#page_hot_src", null ]
    ] ],
    [ "Lab5: Feeling Tipsy", "page_tip.html", [
      [ "Introduction", "page_tip.html#page_tip_int", null ],
      [ "Equation of Motion Derivation", "page_tip.html#page_tip_eom", null ]
    ] ],
    [ "Lab6: Simulation or Reality?", "page_sim.html", [
      [ "Introduction", "page_sim.html#page_sim_int", null ],
      [ "Open Loop Simulink Block Diagram", "page_sim.html#page_sim_olsimlnk", null ],
      [ "Plot 1", "page_sim.html#page_sim_plt1", null ],
      [ "Plot 2", "page_sim.html#page_sim_plt2", null ],
      [ "Plot 3", "page_sim.html#page_sim_plt3", null ],
      [ "Plot 4", "page_sim.html#page_sim_plt4", null ],
      [ "Closed Loop Simulink Block Diagram", "page_sim.html#page_sim_clsimlnk", null ],
      [ "Plot 5", "page_sim.html#page_sim_plt5", null ],
      [ "Source Code", "page_sim.html#page_sim_src", null ]
    ] ],
    [ "Lab7: Feeling Touchy", "page_tch.html", [
      [ "Introduction", "page_tch.html#page_tch_int", null ],
      [ "Source Code", "page_tch.html#page_tch_src", null ],
      [ "Documentation", "page_tch.html#page_tch_doc", null ]
    ] ],
    [ "Lab8&9: Term Project", "page_term.html", [
      [ "Introduction", "page_term.html#page_term_int", null ],
      [ "Gain Value Derivation", "page_term.html#page_term_gain", null ],
      [ "Gain Value Tuning", "page_term.html#page_term_gaint", null ],
      [ "Filtering", "page_term.html#page_term_filt", null ],
      [ "Video Demonstration", "page_term.html#page_term_vid", null ],
      [ "Source Code Access", "page_term.html#page_term_src", null ],
      [ "Documentation", "page_term.html#page_term_doc", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"classTouch__Screen_1_1Touch__Screen.html#aa6c74fe8ca84a3a51f147773ffeea49e"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';