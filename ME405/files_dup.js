var files_dup =
[
    [ "Lab0x09EncoderDriver.py", "Lab0x09EncoderDriver_8py.html", "Lab0x09EncoderDriver_8py" ],
    [ "Lab0x09MotorDriver.py", "Lab0x09MotorDriver_8py.html", "Lab0x09MotorDriver_8py" ],
    [ "Lab0x09TouchScreen.py", "Lab0x09TouchScreen_8py.html", "Lab0x09TouchScreen_8py" ],
    [ "Lab_0x01_FSM.py", "Lab__0x01__FSM_8py.html", [
      [ "TaskVendingMachine", "classLab__0x01__FSM_1_1TaskVendingMachine.html", "classLab__0x01__FSM_1_1TaskVendingMachine" ]
    ] ],
    [ "Lab_0x01_Main.py", "Lab__0x01__Main_8py.html", "Lab__0x01__Main_8py" ],
    [ "Lab_0x02_ThinkFast.py", "Lab__0x02__ThinkFast_8py.html", "Lab__0x02__ThinkFast_8py" ],
    [ "Lab_0x03_Main.py", "Lab__0x03__Main_8py.html", "Lab__0x03__Main_8py" ],
    [ "Lab_0x03_UI.py", "Lab__0x03__UI_8py.html", "Lab__0x03__UI_8py" ],
    [ "Lab_0x04_Main.py", "Lab__0x04__Main_8py.html", "Lab__0x04__Main_8py" ],
    [ "Lab_0x08_EncoderDriver.py", "Lab__0x08__EncoderDriver_8py.html", "Lab__0x08__EncoderDriver_8py" ],
    [ "Lab_0x08_MotorDriver.py", "Lab__0x08__MotorDriver_8py.html", "Lab__0x08__MotorDriver_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "plot.py", "plot_8py.html", "plot_8py" ],
    [ "TermProject.py", "TermProject_8py.html", "TermProject_8py" ],
    [ "Touch_Screen.py", "Touch__Screen_8py.html", "Touch__Screen_8py" ]
];