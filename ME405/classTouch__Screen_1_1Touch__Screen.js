var classTouch__Screen_1_1Touch__Screen =
[
    [ "__init__", "classTouch__Screen_1_1Touch__Screen.html#a56136f07d22dbc9fb1e3b8fcc8bf31b2", null ],
    [ "scanPosition", "classTouch__Screen_1_1Touch__Screen.html#a3dd040b42799e34f7f18e4c6cc47a37e", null ],
    [ "scanX", "classTouch__Screen_1_1Touch__Screen.html#a9e6218f52821333941482bcc43baeb70", null ],
    [ "scanY", "classTouch__Screen_1_1Touch__Screen.html#a9fdcb3c7fc2ab4c1b97cb290d789d1b3", null ],
    [ "scanZ", "classTouch__Screen_1_1Touch__Screen.html#a7bba6a2344032c10a2a3cdb4a22e0d8f", null ],
    [ "center_x", "classTouch__Screen_1_1Touch__Screen.html#a4d509c83f10a3e038f63467e5bee80c5", null ],
    [ "center_y", "classTouch__Screen_1_1Touch__Screen.html#a82a78033d7878c84f04789590ea7801e", null ],
    [ "length", "classTouch__Screen_1_1Touch__Screen.html#ac4ed5e5afa04fb4420bbbf2d45687e03", null ],
    [ "pin_xm", "classTouch__Screen_1_1Touch__Screen.html#ae164a825fface199d5bd72f2928e1013", null ],
    [ "pin_xp", "classTouch__Screen_1_1Touch__Screen.html#ac7030f61d1c955b0fe0f7b94560f0b1b", null ],
    [ "pin_ym", "classTouch__Screen_1_1Touch__Screen.html#a4f4c51e66cc204e776f7403cf6acc314", null ],
    [ "pin_yp", "classTouch__Screen_1_1Touch__Screen.html#a39b2bd8036623489dd6b5a14bd97723c", null ],
    [ "width", "classTouch__Screen_1_1Touch__Screen.html#aa6c74fe8ca84a3a51f147773ffeea49e", null ]
];